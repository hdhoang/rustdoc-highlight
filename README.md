# rustdoc-highlight

A Rust syntax highlighting library. Used by [Rustdoc](https://github.com/rust-
lang/rust/tree/master/src/librustdoc) and [rustw](https://github.com/nrc/rustw),
powered by the lexer from the Rust compiler.

Provides HTML syntax highlighting for any lex-able Rust code. Written in Rust.

This code was originally part of Rustdoc (which is part of the Rust distribution
and [repo](https://github.com/rust-lang/rust)) and has been moved into its own
crate for better reusability.

## Usage

TODO
